import React, { Component } from 'react';
import Button from 'material-ui/Button';
import Recharts from './recharts/Recharts'
import Highcharts from './highcharts/Highcharts';
import Victory from './victory/Victory';
import './App.css';

const initialState = {
  showRecharts: false,
  showHighcharts: false,
  showVictory: false
};

class App extends Component {
  constructor() {
    super();
    this.state = Object.assign({}, initialState);
  }

  toggleCharts(libName, currValue) {
    const newState = Object.assign({}, initialState);
    newState[libName] = !currValue;
    this.setState(newState);
  }

  render() {
    const {showRecharts, showHighcharts, showVictory} = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">JS Charts Demo</h1>
          <Button
            className={"App__button"}
            children={'Recharts'}
            raised={true}
            onClick={() => this.toggleCharts('showRecharts', showRecharts)}
          />
          <Button
            className={"App__button"}
            children={'Victory'}
            raised={true}
            onClick={() => this.toggleCharts('showVictory', showVictory)}
          />
          <Button
            className={"App__button"}
            children={'Highcharts'}
            raised={true}
            onClick={() => this.toggleCharts('showHighcharts', showHighcharts)}
          />
        </header>
        <Recharts displayClass={showRecharts ? "App__show" : "App__hide"}/>
        <Highcharts displayClass={showHighcharts ? "App__show" : "App__hide"}/>
        <Victory displayClass={showVictory ? "App__show" : "App__hide"}/>
      </div>
    );
  }
}

export default App;
