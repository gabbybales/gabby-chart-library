import React from 'react';
import SampleLineChart from './SampleLineChart';
import JSXHighcharts from './JSXHighcharts';
import ComboChart from './ComboChart';
import './Highcharts.css'
import BarDrillDownChart from './BarDrillDownChart';

class Highcharts extends React.Component {
  render() {
    const {displayClass} = this.props;
    return (
      <div className={`${displayClass} highcharts-column`}>
        <h1>Highcharts Example</h1>
        <SampleLineChart/>
        <ComboChart/>
        <BarDrillDownChart/>
        <a href={'http://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/column-drilldown/'}> See Drill down example
        </a>
        <JSXHighcharts />
      </div>
    );
  }
}

export default Highcharts;
