import React from 'react';
import './Victory.css';
import ZoomableLineChart from './ZoomableLineChart';
import PieTimerChart from './PieTimerChart';
import StackedBarChart from './StackedBarChart';

class Victory extends React.Component {
  render() {
    const {displayClass} = this.props;
    return (
      <div className={`victory-charts ${displayClass}`}>
        <h1>Victory Example</h1>
        <StackedBarChart/>
        <PieTimerChart/>
        <ZoomableLineChart/>
      </div>
    );
  }
}

export default Victory;
