import React from 'react';
import LineChartExample from './LineChartExample';
import ScatterChartExample from './ScatterChartExample';
import BarChartExample from './BarChartExample';
import './Recharts.css';

class Recharts extends React.Component {
  render() {
    const {displayClass} = this.props;
    return (
      <div className={displayClass}>
        <h1>Recharts Example</h1>

        <LineChartExample />
        <BarChartExample />
        <ScatterChartExample />
      </div>
    );
  }
}

export default Recharts;
